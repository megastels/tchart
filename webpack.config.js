const path = require('path');

module.exports = function (env) {

	var dev = env !== "prod";

	return {
		context: __dirname,
		entry: './src/index.ts',
		devtool: dev ? "inline-source-map" : "source-map",
		mode: dev ? "development" : "production",
/* 		optimization: {
			minimize: true
		}, */
		module: {
			rules: [
				{
					test: /\.tsx?$/,
					use: 'ts-loader',
					exclude: /node_modules/
				}
			]
		},
		resolve: {
			extensions: ['.tsx', '.ts', '.js']
		},
		devServer: {
			hot: false,
			inline: false,
			port: 3000
		},
		output: {
			filename: 'bundle.js',
			path: path.resolve(__dirname, 'dist')
		}
	};
}
