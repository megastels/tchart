export interface IChartConf{
	/**Толщина линий */
	lineSize?:number;
	/**Минимальное расстояние между точками на оси Х */
	xaxisSegmentsDistance?:number;

	/**количество линий по оси Y 
	 * depricated
	*/
	yaxisLinesCount?:number;

	/**night mode */
	nightMode?:boolean;
	/**При true, используется css transition для анимации графиков
	 * Более плавня анимация. 
	 * Работает только в Chrome
	 * При значении false графики анимируются скриптом.
	 * Возможна небольшая рассинхронизация движения.
	 */
	useNativeAnimations?:boolean;
	/**Скорость анимации */
	animationSpeed?:number;
	
	/**Определяет размер viewBox для SVG */
	viewWidth?:number;
	/**Определяет размер viewBox для SVG */
	viewHeight?:number;
	previewHeight?:number;
}