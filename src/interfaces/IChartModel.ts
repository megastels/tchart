type ns = number | string;
export interface IChartModel{
	colors:any;
	columns:any[][];
	names:any;
	types:{
		x:string,
		[key:string]:string
	};
}