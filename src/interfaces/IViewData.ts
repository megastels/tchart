export enum ViewChangeType {
	Move,
	ResizeLeft,
	ResizeRight
}
export interface IViewData {
	ev: MouseEvent | TouchEvent;
	delta: number;
	view: any;
	shift: number;
	size: number;
	type: ViewChangeType;
	[key: string]: any;
}