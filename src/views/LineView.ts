import { Line } from "../elements/Line";
import { getMax } from "../common/utils";
import { ILineViewState } from "../interfaces/ILineViewState";
import { View } from "../common/View";
//import { SimpleLine } from "./___SimpleLine";
import { IChartModel } from "../interfaces/IChartModel";
import { XAxis2 } from "../elements/XAxis";
import { IChartConf } from "../interfaces/IChartConf";
import { InfoLine } from '../elements/InfoLine';
import { YAxis } from '../YAxis';

export class LineView extends View {
	lines: Line[] = [];
	xaxis: XAxis2 = null;//new XAxisLine();
	yaxis: YAxis = null;
	public infoLine:InfoLine = null;//new Line();


	constructor(w: number, h: number, model: IChartModel, conf:IChartConf) {
		super(w, h - 40);

		this.xaxis = new XAxis2(model.columns[0].slice(1), w, conf);
		this.yaxis = new YAxis(conf);
		this.appendChild(this.xaxis);
		this.appendChild(this.yaxis);
		model.columns.slice(1).forEach(item => {
			var name = item[0];
			var points = item.slice(1);
			this.addLine(points, name, model.colors[name], conf);
		});

		this.xaxis.normalize(this.deltaX);
		//this.yaxis.normalize();

		this.infoLine = new InfoLine(this.height, this.lines, this.xaxis);
		this.infoLine.visible = false;
		this.update();

		this.appendChild(this.infoLine);

	}

	/**largest line length */
	get linesMaxWidth() {
		return getMax(this.lines.filter(l => l.visible).map(l => l.points.length));
	}

	get linesMaxHeight() {
		var viewPoints = this.viewPoints;
		var start = viewPoints[0];
		var end = viewPoints[viewPoints.length - 1];

		return getMax([].concat(...this.lines.filter(l => l.visible).map(l => l.points.slice(start, end))));
	}

	private _deltaX = 1;
	get deltaX() {
		return this._deltaX;//this.width / this.linesMaxWidth;
	}

	get deltaY() {
		return this.height / this.linesMaxHeight;
	}

	get viewPoints() {
		var points = [];
		var max = this.linesMaxWidth;
		var delta = this.deltaX;

		var c = false;
		for (var i = 0; i < max; i++) {
			var item = i * delta * this.scaleX - this.translateX;

			if (item >= 0 && item < this.width) {
				points.push(i);
				c = true;
			}

			if (c && item > this.width) {
				points.push(i);
				break;
			}
		}

		if (points[0] !== 0 && points.length > 0) {
			points.unshift(points[0] - 1);
		}

		return points;
	}

	hideInfoLine(){
		this.infoLine.visible = false;
	}
	showInfoLine(position){
		var scale = this.scaleX;
		var delta = this.deltaX
		var translate = this.translateX;
		var width = this.width;

		var viewWidth = width * scale;
		var realPosition = position * viewWidth - translate;
		var d = delta*scale/2;
		

		var candidate = null;
		var points = this.xaxis.segments.points;

		for(var i=0; i<points.length; i++){
			let p = points[i];
			let next = points[i+1];

			if(i+1<points.length){
				//TODO
				//console.log(state, p.position[0])

				if(realPosition>p.position[0]-d && realPosition<next.position[0]-d){
					candidate = i;
					break;
				}
			}else{
				candidate = i;
			}
		}
		this.infoLine.visible = true;
		this.infoLine.index = candidate;
		this.infoLine.update(this.deltaX * this.scaleX, this.translateX)
	}

	changeViewState(state: ILineViewState) {
		var newScale = Math.pow(state.size, -1);
		this.translateX = state.shift * this.width * newScale;
		this.scaleX = newScale;
		this.update();
	}

	addLine(points: number[], name: string = "untitled", color: string = "gray", conf:IChartConf) {
		var line = new Line(conf.useNativeAnimations, conf.animationSpeed);
		line.name = name;
		line.color = color;
		line.points = points;
		line.size = conf.lineSize;

		this.appendChild(line);
		this.lines.push(line);

		this._deltaX = this.width / this.linesMaxWidth;

		return line;
	}


	public clear() {
		this.lines = [];
		super.clear();
	}

	public update() {
		var deltaX = this.deltaX;
		var deltaY = this.deltaY;

		//console.log(deltaX);
		this.xaxis.update(deltaX * this.scaleX, this.translateX);
		this.yaxis.update(this.linesMaxHeight, deltaY);


		this.lines.forEach(l => {
			l.animatedDataPath = l.points.map((value, index) => {
				return [
					//xpoints[index],
					index * deltaX * this.scaleX - this.translateX,
					value * deltaY
				];
			});
		});
		this.infoLine.update(deltaX * this.scaleX, this.translateX);
	}
}