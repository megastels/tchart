import { LineView } from "./LineView";
import { Line } from "../elements/Line";
import { View } from "../common/View";
import { getMax } from "../common/utils";
export class Preview extends View {
	lines: Line[] = [];
	constructor(private targetView: LineView, width: number, height: number) {
		super(width, height);

		this.targetView.lines.forEach(l => {
			var nl = new Line();
			nl.name = l.name;
			nl.color = l.color;
			nl.size = l.size;
			nl.points = l.points;
			this.lines.push(nl);
			this.appendChild(nl);
		});
		this.update();
	}

	/**largest line length */
	get linesMaxWidth() {
		return getMax(this.lines.filter(l => l.visible).map(l => l.points.length));
	}
	get linesMaxHeight() {
		var viewPoints = this.viewPoints;
		var start = viewPoints[0];
		var end = viewPoints[viewPoints.length - 1];
		return getMax([].concat(...this.lines.filter(l => l.visible).map(l => l.points.slice(start, end))));
	}
	get deltaX() {
		return this.width / this.linesMaxWidth;
	}
	get deltaY() {
		return this.height / this.linesMaxHeight;
	}
	get viewPoints() {
		var points = [];
		var max = this.linesMaxWidth;
		for (var i = 0; i < max; i++) {
			points.push(i);
		}
		return points;
	}
	public update() {
		var deltaX = this.deltaX;
		var deltaY = this.deltaY;
		this.lines.forEach(l => {
			l.dataPath = l.points.map((value, index) => {
				return [
					index * deltaX,
					value * deltaY
				];
			});
		});
	}
}
