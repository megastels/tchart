import { LineView } from "./LineView";
import { SVGNode } from "../common/SvgNode";
import { MapViewControl } from "../classes/MapViewControl";
import { Preview } from "./Preview";

export class MapView{
	public target = document.createElement("div");
	private _svg:SVGNode = new SVGNode("svg");
	private _preview:Preview = null;
	private _control:MapViewControl = new MapViewControl();

	constructor(private targetView:LineView, width:number, height:number){
		this._svg.attr("viewBox", `0 0 ${width} ${height}`);
		this._preview = new Preview(targetView, width, height);
		this._svg.appendChild(this._preview);
		this.target.appendChild(this._svg.target);
		this.target.className = "preview";
		this.target.appendChild(this._control.target);

		this._control.onchange = (vals)=>{
			this.targetView.changeViewState(vals);
		}
	}

	get busy(){
		return this._control.busy;
	}

	get scale(){
		return this._control.size;
	}
	get shift(){
		return this._control.shift;
	}

	get lines(){
		return this._preview.lines;
	}
}