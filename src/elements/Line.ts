import { Path } from "../common/Path";
import { CAnimation } from "../classes/CAnimation";


export class Line extends Path{
	
	protected _animation:CAnimation = null;

	constructor(private useNativeAnimation:boolean = false, animationSpeed:number = 300){
		super();

		if(useNativeAnimation){
			this.target.style.transition = `all ${animationSpeed}ms linear`;
		}else{
			this.target.style.transition = `opacity ${animationSpeed}ms linear`;
		}

		this._animation = new CAnimation(this._onFrame, animationSpeed);
	}

	protected _dataPath:number[][] = [];


	get dataPath(){
		return this._dataPath;
	}
	set dataPath(val:number[][]){
		this._dataPath = val;

		this.d = this.convertDataPath(val);
	}



	private _toState:any = null;
	private _curState:any = null;
	set animatedDataPath(val:number[][]){
		if(this.useNativeAnimation || this._dataPath.length==0){
			this.dataPath = this._curState = val;
			return;
		}
		
		this._dataPath = this._curState.slice();
		this._toState = val;
		this._animation.start();
	}


	_test = [];
	protected _onFrame = t=>this._frame(t);
	protected _frame(progress){
		var arrFrom = this._dataPath;
		var arrTo = this._toState;

 		var res = arrFrom.map((from, index)=>{
			var to = arrTo[index];
			var val = [
				from[0]+(to[0]-from[0])*progress,
				from[1]+(to[1]-from[1])*progress
			];
			this._curState[index] = val;
			

			if(index===0){
				return `M${val.join(",")}`;
			}else if(index===1){
				return `L${val.join(",")}`;
			}else{
				return val.join(",");
			}
		}).join(" "); 

		if(progress===1){
			this._dataPath = this._toState;
		}

		this.d = res;
	}


	convertDataPath(val:number[][]){
		return val.map((val, index)=>{
			if(index===0){
				return `M${val.join(",")}`;
			}else if(index===1){
				return `L${val.join(",")}`;
			}else{
				return val.join(",");
			}
		}).join(" ");
	}
	
}

/* var div = document.createElement("div");
div.style.position = "fixed";
div.style.width = "5px";
div.style.height = "5px";
div.style.left = "0px;";
div.style.top = "0px;";
div.style.background = "black";
div.style.color = "white";
document.body.appendChild(div);


var from = [0, 0];
var to = [0, 0];
var state = [0, 0];

var a = new CAnimation(function(progress){
	console.log(progress);

	state = [
		from[0]+(to[0]-from[0])*progress,
		from[1]+(to[1]-from[1])*progress
	]
	div.style.left = `${state[0]}px`;
	div.style.top = `${state[1]}px`;

}, 1000);
a.start();

window.addEventListener("mousemove", ev=>{
	from = state;
	to = [ev.x, ev.y];
	a.start();
	for(var i=0; i<100000000; i++){
		i=i+100/100-1;
	}

	

	//console.log(123);
});
 */