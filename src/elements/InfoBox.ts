import { createNode } from '../common/utils';

export class InfoBox{
	target = createNode("div");
	dateInfo = createNode("div", this.target);
	listInfo = createNode("ul", this.target);

	constructor(){
		this.target.classList.add("info-box");
		this.dateInfo.classList.add("title");
		this.visible = false;


		createNode("li", this.listInfo);
	}

	get title(){
		return this.dateInfo.innerText;
	}
	set title(v:string){
		this.dateInfo.innerText = v;
	}


	set visible(v:boolean){
		this.target.style.display = v?"":"none";
	}
	get visible(){
		return this.target.style.display === "";
	}

	private _position:number[];
	get position(){
		return this._position;
	}
	set position(v:number[]){
		this._position = v;
		this.target.style.left = `${v[0]}px`;
		this.target.style.top = `${v[1]}px`;
	}

	getRect(){
		//console.log(this.target.getClientRects());
		return this.target.getClientRects()[0];
	}

	setInfo(arrInfo:{value:number, color:string, name:string}[]){
		var list = this.listInfo.children;
		if(list.length<arrInfo.length){
			var to = arrInfo.length - list.length
			for(var i=0; i<to; i++){
				createNode("li", this.listInfo);
			}
		}

		arrInfo.forEach((item, i)=>{
			var li = <HTMLElement>list[i];
			li.style.color = item.color;
			li.innerHTML = `<span class='val'>${item.value}</span><br/><span class='name'>${item.name}</span>`;
		})
	}
}