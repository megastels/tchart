import { Line } from "./Line";
import { SVGNode } from "../common/SvgNode";
import { XAxisSegment } from "./XAxisSegment";
import { IChartConf } from "../interfaces/IChartConf";


/**Более производительный вариант. */
export class XAxis2 extends SVGNode{
	private mainline:Line = new Line();
	public segments:XAxisSegmentList = null;
	constructor(points:number[], width:number, private conf:IChartConf){
		super("g");

		this.classList.add("xaxis");

		this.mainline.dataPath = [[0, 0], [width, 0]];
		this.segments = new XAxisSegmentList(points, conf);

		this.appendChild(this.mainline);
		this.appendChild(this.segments);
	}

	normalize(delta){
		this.segments.normalize(delta);
	}

	update(delta, translate=0){
		this.segments.update(delta, translate);
	}

	set position(val){
		this.segments.position = val;
	}
	get position(){
		return this.segments.position;
	}
}

export class XAxisSegmentList extends SVGNode{
	points:XAxisSegment[] = [];
	eq:number = 1;
	minDistance:number = 100;
	private length:number = 0;
	constructor(points:number[], conf:IChartConf){
		super("g");
		this.length = points.length;

		this.minDistance = conf.xaxisSegmentsDistance;

		points.forEach((val, i)=>{
			var segment = new XAxisSegment(10, val, conf.animationSpeed);
			segment.setParent(this);
			segment.position = [i, 0];
			this.points.push(segment);
			this.appendChild(segment);
		});
	}

	up(){
		var delta = this.eq;
		this.eq*=2;
		for(var i=this.eq-1; i-delta<this.points.length; i+=this.eq){
			this.points[i-delta].visible = false;
		}
	}
	down(){
		if(this.eq>1){
			this.eq/=2;
			this.each(s=>{
				s.visible = true;
			})
		}
	}

	each(cb:(s:XAxisSegment, i:number)=>void){
		for(var i=this.eq-1; i<this.points.length; i+=this.eq){
			cb(this.points[i], i);
		}
	}

	normalize(delta){
		var res = delta*this.eq;
		while(res<this.minDistance){
			this.up();
			res = delta*this.eq;
		}
	}

	update(delta, translate = 0){
		var res = delta*this.eq;
		
		if(res<this.minDistance){
			this.up();
		}else if(res>this.minDistance*2){
			this.down();
		}

		this.points.forEach((p, i)=>{
			p.position = [i*delta-translate, 0];
		})
	}
}