import { Text } from "../common/Text";
export class DateText extends Text {
	private date: Date;
	private monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept", "Oct", "Nov", "Dec"];
	private weekNames = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];

	constructor(date: Date, reverse?: boolean);
	constructor(date: number, reverse?: boolean);
	constructor(date: number | Date, reverse: boolean = false) {
		super(reverse);
		this.classList.add("date-text");
		if (typeof (date) == "number") {
			date = new Date(date);
		}
		this.date = date;
		this.value = `${this.month} ${this.day}`;
	}
	get month() {
		return this.monthNames[this.date.getMonth()];
	}
	get year() {
		return this.date.getFullYear();
	}
	get day() {
		return this.date.getDate();
	}
	get weekDay(){
		return this.weekNames[this.date.getDay()];
	}
}
