import { SVGNode } from "../common/SvgNode";
import { Path } from "../common/Path";
import { DateText } from "./DateText";
import { debounce } from "../common/utils";
import { XAxisSegmentList } from "./XAxis";

export class XAxisSegment extends SVGNode{
	private segment = new Segment();
	public text:DateText = null;
	private removeInvisible:any = null;

	constructor(segmentSize:number = 10, public date:number, private speed:number){
		super("g");
		this.text = new DateText(date, true);
		this.text.position = [0, segmentSize+20];

		this.appendChild(this.segment);
		this.appendChild(this.text);
		this.classList.add("xaxis-segment");
		this.target.style.transition = `transform ${speed}ms linear, opacity ${speed}ms linear`

		this.removeInvisible = debounce(()=>{
			if(this._visible===false)
				this.remove();
		}, speed);
	}

	set visible(val:boolean){
		if(this._visible === val)
			return;

		this._visible = val;

		if(val){
			if(this._removed){
				this.restore();
			}else{
				this.target.style.opacity = "1";
			}
		}else{
			this.target.style.opacity = "0";
			this.removeInvisible();
		}
	}

	private _parent:XAxisSegmentList = null;
	private _removed:boolean = true;
	setParent(parent){
		this._parent = parent;
	}
	restore(){
		if(this._parent){
			this._parent.appendChild(this);
			this._removed = false;
 			setTimeout(_=>{
				if(this._visible)
					this.target.style.opacity = "1";
			}, 20) 
		}
	}

	remove(){
		this.target.remove();
		this._removed = true;
	}
}

export class Segment extends Path{
	constructor(segmentSize:number = 10){
		super();

		var x = 0;
		var y = 0;

		this.classList.add("segment");

		var current = `Q ${x},${y}`
		var first = `M ${x-segmentSize}, ${y}`;
		var middle = `${x}, ${y-segmentSize}`;
		var last = `${x+segmentSize}, ${y}`;

		this.d = `${first} ${current} ${middle} ${current} ${last}`;
	}
	
	
	public get displayed() : boolean {
		return this.target.style.display !== "none";
	}
	
	public set displayed(v : boolean) {
		this.target.style.display = v?"":"none";
	}
	
}