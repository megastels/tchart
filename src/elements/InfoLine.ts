import { SVGNode } from '../common/SvgNode';
import { Line } from './Line';
import { Circle } from '../common/Circle';
import { InfoBox } from './InfoBox';
import { XAxis2 } from './XAxis';

export class InfoLine extends SVGNode{
	line:Line = new Line();
	circles:Circle[] = [];

	infoBox:InfoBox = null;

	index:number = 0;

	constructor(h:number, private lines:Line[], private xaxis:XAxis2){
		super("g");

		this.line.dataPath = [[0, 0], [0, h]];
		this.appendChild(this.line);
		this.lines.forEach(l=>{
			var c = new Circle();
			c.color = l.color;
			this.circles.push(c);
			this.appendChild(c);
		});
	}

	update(scale, translate){
		this.position = [this.index * scale - translate, 0];

	 	this.lines.forEach((l, i)=>{
			var circle = this.circles[i];
			var position = l.dataPath[this.index]||[0,0];

			circle.visible = l.visible;
			circle.position = [0, position[1]];
		});

		if(this.infoBox){
			var dateText = this.xaxis.segments.points[this.index].text;
			this.infoBox.title = `${dateText.weekDay} , ${dateText.month} ${dateText.day}`;
			var data = this.lines.map(l=>{
				return {
					color:l.color,
					value:l.points[this.index],
					name:l.name
				}
			});
			this.infoBox.setInfo(data)
		}
	}

	attachInfoBox(box:InfoBox){
		this.infoBox = box;

	}
}