import { Chart } from "./Chart";
import { IChartModel } from "./interfaces/IChartModel";

function createButton(name, cb) {
	var btn = document.createElement("button");
	btn.classList.add("chart-ctrl");
	btn.innerText = name;
	btn.onclick = cb;

	return btn;
}

fetch("./data_chart.json")
	.then(res => res.json())
	.then((data: IChartModel[]) => {
		console.log(data);
		data.forEach((conf, index) => {
			var chart = new Chart(conf, {
				lineSize: 2,
				nightMode: false,
				useNativeAnimations:false,
				animationSpeed:300,

				viewWidth:1000,
				viewHeight:562,
				previewHeight:56,
				xaxisSegmentsDistance:150,
			});

			chart.appendTo(document.body);

			var btn = createButton("switch theme", function () {
				chart.swithTheme();
			});
			document.body.appendChild(btn);

			chart.lines.forEach(function (l) {
				var lb = createButton(l.name, function () {
					l.visible = !l.visible;
					lb.innerText = `${l.visible ? "hide" : "show"} ${l.name}`;
				});
				lb.innerText = `${l.visible ? "hide" : "show"} ${l.name}`;
				document.body.appendChild(lb);
			})
		})
	});