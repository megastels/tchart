import { SVGNode } from "./common/SvgNode";
import { Line } from "./elements/Line";
import { IChartConf } from './interfaces/IChartConf';
import { Text } from "./common/Text";
import { debounce } from './common/utils';


export class YAxis extends SVGNode {
	lines: YAxisLine[] = [];
	height: number = 0;

	constructor(private conf: IChartConf) {
		super("g");
		this.classList.add("yaxis");
	}

	private lastDelta = 0;
	private lastMax = 0;
	update(max, delta) {
		//console.log(max, delta);

		if (max !== this.lastMax) {
			var newLines = this.createLines(max, this.lastDelta);
			if(newLines === null){
				this.lines.forEach((l, i) => {
					l.update(delta);
				});
			}else{
				this.lines.forEach((l, i) => {
					l.visible = false;
					l.update(delta);
				});
				
				setTimeout(_=>{
					newLines.forEach(l => {
						l.update(delta);
						l.visible = true;
					})
				}, 20)
	
				this.removeLines(this.lines);
				this.lines = newLines;
			}

			this.lastMax = max;
			this.lastDelta = delta;
		}
	}

	removeLines(items){
 		setTimeout(_=>{
			items.forEach(item => {
				item.remove();
			});
		}, this.conf.animationSpeed); 
	}


	private hz(val){
		if(val<10){
			if(val>5)
				return 5;
			return 1;
		}

		var length = val.toString().length;
		var l = Math.ceil(length/2);
		var r = Math.pow(10, l);
	
		return Math.floor(val / r) * r;
	}

	private _lastLen = 0;
	private _last = 0;
	createLines(h: number, delta) {
		var res = [];
		const cnt = 5;
		var t = this.hz(Math.floor(h / cnt));
		var len = Math.ceil(h/t);
		if(this._last === t){
 			if(len > this._lastLen){
				for(var i=this._lastLen; i<len; i++){
					var l = this.createLine(t * i, delta);
					//console.log("add line", l);
					l.visible = true;
					this.lines.push(l);
					this.appendChild(l);
				}
				this._lastLen = len;
			} 
			

			return null;
		}
		this._last = t;
		this._lastLen = len;


		for (var i = 0; i < len; i++) {

			var l = this.createLine(t * i, delta);
			res.push(l); 
			this.appendChild(l);
		}

		return res;
	}

	createLine(value, delta){
		var l = new YAxisLine(this.conf.viewWidth, this.conf);

		l.value = value;
		l.update(delta);
		l.visible = false;

		return l;
	}

}



export class YAxisLine extends SVGNode {
	text: Text = new Text(true);
	line: Line = new Line();


	constructor(w: number, conf: IChartConf) {
		super("g");
		this.line.dataPath = [[0, 0], [w, 0]];
		this.text.anchor = "left";

		this.target.style.transition = `all ${conf.animationSpeed}ms linear`;

		this.appendChild(this.text);
		this.appendChild(this.line);
	}


	private _value = 0;
	get value(): number {
		return this._value;
	}

	set value(v: number) {
		this._value = v;
		this.text.value = v.toString();
		this.update();
	}

	private _delta = 1;
	update(delta = 1) {
		this._delta = delta;
		this.position = [0, this.value * delta];
	}
}