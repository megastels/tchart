import { SVGNode } from "./common/SvgNode";
import { IChartModel } from "src/interfaces/IChartModel";
import { LineView } from "./views/LineView";
import { MapView } from "./views/MapView";
import { LineControl } from "./classes/LineControl";
import { IChartConf } from "./interfaces/IChartConf";
import { InfoBox } from './elements/InfoBox';

export class Chart {
	private _viewSize = {
		width: 1000,
		height: 562,
		previewHeight: 56
	}

	private _node = document.createElement("div");
	private _svg: SVGNode = new SVGNode("svg");

	private _lineView: LineView = null;
	private _mapView: MapView = null;
	private _infoBox: InfoBox = null;


	public lines: LineControl[] = [];

	constructor(model: IChartModel, c:IChartConf = {}) {
		var conf:IChartConf = {
			lineSize:c.lineSize||2,
			nightMode:c.nightMode||false,
			useNativeAnimations:c.useNativeAnimations||false,
			animationSpeed:c.animationSpeed||300,
			xaxisSegmentsDistance:c.xaxisSegmentsDistance||150,
			yaxisLinesCount:c.yaxisLinesCount||5,
			viewWidth:c.viewWidth||1000,
			viewHeight:c.viewHeight||562,
			previewHeight:c.previewHeight||56
		}

		this._viewSize.width = conf.viewWidth;
		this._viewSize.height = conf.viewHeight;
		this._viewSize.previewHeight = conf.previewHeight;

		this._node.classList.add("chart");
		if(conf.nightMode){
			this._node.classList.add("night_mode");
		}

		this._svg.attr("viewBox", `0 0 ${this._viewSize.width} ${this._viewSize.height}`);

		this._lineView = new LineView(this._viewSize.width, this._viewSize.height, model, conf);
		this._mapView = new MapView(this._lineView, this._viewSize.width, this._viewSize.previewHeight);

		this._lineView.lines.forEach((l, index) => {
			var lc = new LineControl(l.name, this._lineView, this._mapView);
			this.lines.push(lc);
		});


		this._infoBox = new InfoBox();
		this._lineView.infoLine.attachInfoBox(this._infoBox);

		this._svg.appendChild(this._lineView);
		this._node.appendChild(this._svg.target);
		this._node.appendChild(this._mapView.target);
		this._node.appendChild(this._infoBox.target);

		this._svg.target.addEventListener("mouseenter", this._onmouseenter);
		this._svg.target.addEventListener("touchstart", this._ontouchstart);
		this._svg.target.addEventListener("touchmove", this._ontouchemove);
		this._svg.target.addEventListener("touchend", this._ontouchend);
		//this.drawDevGrid();
	}

	private _onmouseenter = ev=>this._mouseenter(ev);
	private _rect = null;
	private _mouseenter(ev){
		this._svg.target.addEventListener("mousemove", this._onmousemove);
		this._svg.target.addEventListener("mouseleave", this._onmouseleave);
		this._rect = this._svg.target.getClientRects()[0];
		//this._infoBox.visible = true;
	}
	private _onmousemove = ev=>this._mousemove(ev);
	private _mousemove(ev){
		var rect = this._rect;
		var x = ev.x - rect.left;
		this._move(rect, x);
	}
	private _onmouseleave = ev=>this._mouseleave(ev);
	private _mouseleave(ev){
		this._svg.target.removeEventListener("mousemove", this._mousemove);
		this._svg.target.removeEventListener("mouseleave", this._onmouseleave);
		this._lineView.hideInfoLine();
		this._infoBox.visible = false;
	}


	private _ontouchstart = ev=>this._touchstart(ev);
	private _touchstart(ev){
		this._rect = this._svg.target.getClientRects()[0];
		//this._infoBox.visible = true;
		this._ontouchemove(ev);
	}

	private _ontouchemove = ev=>this._touchmove(ev);
	private _touchmove(ev:TouchEvent){
		var rect = this._rect;
		var touch = ev.touches[0];
		var x = touch.pageX - rect.left;
		this._move(rect, x);

	}

	private _ontouchend = ev=>this._touchend(ev);
	private _touchend(ev){
		this._lineView.hideInfoLine();
		this._infoBox.visible = false;
	}

	private _move(rect, x){
		if(!this._mapView.busy){
			this._infoBox.visible = true;

			this._lineView.showInfoLine(x/rect.width * this._mapView.scale + this._mapView.shift);

			var r = this._infoBox.getRect();
			var infoWidth = 100;
			if(r){
				infoWidth = r.width;
			}

			var shift = x-20;

			if(shift<20){
				shift=20;
			}		
			if(infoWidth+x>rect.width){
				shift = rect.width - infoWidth;
			}

			this._infoBox.position = [shift, 10];
		}
	}




	swithTheme() {
		this._node.classList.toggle("night_mode");
	}



	private drawDevGrid() {
		var grid = new SVGNode("g");

		for (let i = 0; i < this._viewSize.width; i += 5) {
			let p = new SVGNode("path");
			p.classList.add("grid");
			p.attr("d", `M${i},0 L${i}, ${this._viewSize.height}`);
			grid.appendChild(p);
		}
		for (let i = 0; i < this._viewSize.height; i += 5) {
			let p = new SVGNode("path");
			p.classList.add("grid");
			p.attr("d", `M0,${i} L${this._viewSize.width},${i}`);
			grid.appendChild(p);
		}

		this._svg.appendChild(grid);
	}

	appendTo(node: Element) {
		node.appendChild(this._node);
	}
}

