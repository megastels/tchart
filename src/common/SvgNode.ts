import { getID } from "./utils";

export class SVGNode{
	public target:SVGElement;

	constructor(data:string)
	constructor(data:SVGElement)
	constructor(data:any){
		if(typeof data=="string")
			this.target = document.createElementNS("http://www.w3.org/2000/svg", data);
		else
			this.target = data;

		this.id = getID();
	}

	get id(){
		return this.target.id;
	}
	set id(val:string){
		this.target.id = val;
	}

	get classList(){
		return this.target.classList;
	}

	protected _visible = true;
	get visible(){
		return this._visible;
	}
	set visible(val:boolean){
		this._visible = val;
		//this.attr("opacity", val?"1":"0");
		this.target.style.opacity = val?"1":"0";
	}

	protected _position:number[] = [0, 0];
	get position(){
		return this._position;
	}
	set position(val:number[]){
		this._position = val;
		//this.attr("transform", `translate(${val[0]} ${val[1]})`);
		this.target.style.transform = `translate(${val[0]}px, ${val[1]}px)`;
		//console.log(this.target.style.transform);
	}

	attr(name:string):string
	attr(name:string, val:string):void
	attr(name:string, val?:string){
		if(val===undefined){
			return this.target.getAttribute(name);
		}else{
			this.target.setAttribute(name, val);
		}
	}

 	appendChild(node:SVGNode){
		this.target.appendChild(node.target);
	}

	clear(){
		this.target.innerHTML = "";
	}

	remove(){
		this.target.remove();
	}
}