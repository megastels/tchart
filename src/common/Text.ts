import { SVGNode } from "./SvgNode";
export class Text extends SVGNode {
	constructor(reverse: boolean = false) {
		super("text");
		
		this.color = "gray";
		
		if (reverse)
			this.attr("transform", "matrix(1, 0, 0, -1, 0, 0)");
		this.anchor = "middle";
	}

	
	public set color(v : string) {
		this.attr("fill", v);
	}
	public get color() {
		return this.attr("fill");
	}

	get anchor(){
		return this.attr("text-anchor");
	}
	set anchor(val:string){
		this.attr("text-anchor", val);
	}

	get value() {
		return this.target.textContent;
	}
	set value(val: string) {
		this.target.textContent = val;
	}

	get position() {
		return this._position;
	}
	set position(val: number[]) {
		this._position = val;
		this.attr("x", val[0].toString());
		this.attr("y", val[1].toString());
	}
}

