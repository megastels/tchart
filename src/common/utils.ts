var cnt = 1;
export function getID() {
	return `n${cnt++}`;
}

export function getMax(arr: number[]) {
	return Math.max(...arr);
}

export function createNode(name: string, parent?: HTMLElement, className?: string) {
	var node = document.createElement(name);
	if (parent) {
		parent.appendChild(node);
	}
	if (className)
		node.className = className;
	return node;
}
export function randomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function debounce(cb, time) {
	let timer = null;

	return function (...args) {
		const onComplete = () => {
			cb.apply(this, args);
			timer = null;
		}

		if (timer) {
			clearTimeout(timer);
		}

		timer = setTimeout(onComplete, time);
	};
}