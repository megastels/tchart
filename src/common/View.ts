import { SVGNode } from "./SvgNode";



export abstract class View extends SVGNode{
	constructor(
		public readonly width:number, 
		public readonly height:number, 
		public readonly useTransform:boolean = false

		){
		super("g");
		// reverse YAxis
		this.attr("transform", `matrix(1 0 0 -1 0 ${height})`);
	}

	private _scaleX:number = 1;
	set scaleX(val:number){
		this._scaleX = val;
		//this.update();
	}
	get scaleX(){
		return this._scaleX;
	}

	private _translateX:number = 0;
	set translateX(val:number){
		this._translateX = val;
		//this.update();
		//this.position = [val, 0];

		//console.log(this.position);
	}
	get translateX(){
		return this._translateX;
	}

	set position(val){
		this.attr("transform", `matrix(1 0 0 -1 ${-val[0]} ${this.height+val[1]})`);
	}

	public abstract update():void;
}