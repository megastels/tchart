import { SVGNode } from './SvgNode';

export class Circle extends SVGNode{
	constructor(){
		super("circle");
		this.radius = 6;
		this.position = [0,0]
		this.color = "gray";
		this.classList.add("circle");
	}
	
	set color(v : string) {
		this.attr("stroke", v);
	}
	
	get color(){
		return this.attr("stroke");
	}
	

	set position(val:number[]){
		this._position = val;
		this.attr("cx", val[0].toString())
		this.attr("cy", val[1].toString())
	}

	get radius(){
		return +this.attr("r");
	}
	set radius(val:number){
		this.attr("r", val.toString());
	}
}