import { SVGNode } from "./SvgNode";
/**
 * На ум приходит несколько вариантов анимировать эту красоту.
 * 1) Использовать css transition. 
 * Плюсы
 * Простой способ
 * Максимальная плавность графики.
 * Минусы
 * Плохая поддержка браузерами (реально, работает только в хроме и хромоподобных браузерах (opera, yandex...))
 * 
 * 2) css transition + css transform
 * Поюсы
 * Простой способ
 * Максимальная плавность графики.
 * Хорошая браузерная поддержка.
 * Минусы
 * Из-за трансформаций, графики будет колбасить. При большом масштабировании горизонтальные и вертикальные линии будут разного размера. 
 * 
 * 3) Использовать SVG SMIL анимации
 * Плюсы. 
 * Теоретически их можно будет на любой платформе использовать. (Но я не уверен, т.к. не работал с Android/IOS)
 * Минусы
 * Сложная реализация
 * Да и в целом, не очень привлекательный вариант
 * 
 * 4) Самописные анимации или библиотеки
 * Плюсы
 * Работает всегда и везде
 * Минусы
 * Плавность графики для большого объема данных
 */
export class Path extends SVGNode{
	public name:string;
	public points:number[];

	constructor(){
		super("path");
		this.color = "gray";
		this.size = 1;
		this.fill = "none";
	}

	get d(){
		return this.attr("d");
	}
	set d(val:string){
		this.attr("d", val);
	}

	get color(){
		return this.attr("stroke");
	}
	set color(val:string){
		this.attr("stroke", val);
	}

	get size(){
		return parseFloat(this.attr("stroke-width"));
	}
	set size(val:number){
		this.attr("stroke-width", val.toString());
	}

	get fill(){
		return this.attr("fill");
	}
	set fill(val:string){
		this.attr("fill", val);
	}
}