import { ILineViewState } from "../interfaces/ILineViewState";
import { createNode } from "../common/utils";
import { IViewData, ViewChangeType } from "../interfaces/IViewData";



export class MapViewControl {
	public target: HTMLElement = createNode("div");
	public minSize = 0.05;
	public busy:boolean = false;


	private _visibleView: HTMLElement = null;
	private _shiftView: HTMLElement = null;
	private _resizeLeft: HTMLElement = null;
	private _resizeRight: HTMLElement = null;

	constructor() {
		this.target.className = "view-map";
		this._shiftView = createNode("div", this.target, "item shift");
		this._visibleView = createNode("div", this.target, "item view");

		this._resizeLeft = createNode("div", this._visibleView, "res res-left");
		this._resizeRight = createNode("div", this._visibleView, "res res-right");


		createNode("div", this.target, "item");
		this.addListeners();
		this.size = 1;
		//this._visibleView.style.flexBasis = `${this._size * 100}%`;
	}
	public onchange: (state: ILineViewState) => any = null;


	private _shift: number = 0;
	get shift() {
		return this._shift;
	}
	set shift(val: number) {
		if (val < 0)
			val = 0;
		if (val + this.size > 1) {
			val = 1 - this.size;
		}
		this._shift = val;
		
		this._shiftView.style.flexBasis = `${this._shift * 100}%`;
		//this.update();
	}
	private _size: number = 0;
	get size() {
		return this._size;
	}
	set size(val: number) {
		if (val < this.minSize)
			val = this.minSize;
		if (val + this.shift > 1) {
			val = 1 - this.shift;
		}
		this._size = val;
		this._visibleView.style.flexBasis = `${this._size * 100}%`;

		//this.update();
	}


	private move(state: IViewData) {
		var { type, delta } = state;
		var { Move, ResizeLeft, ResizeRight } = ViewChangeType;
		switch (type) {
			case Move:
				var newShift = delta / this.viewData.view.width + this.viewData.shift;
				this.shift = newShift;
				break;
			case ResizeLeft:
				var newShift = delta / this.viewData.view.width + this.viewData.shift;
				var newSize = -delta / this.viewData.view.width + this.viewData.size;

				if (newSize >= this.minSize) {
					this.shift = newShift;
					this.size = newSize;
				}
				break;
			case ResizeRight:
				var newSize = delta / this.viewData.view.width + this.viewData.size;
				this.size = newSize;
				break;
		}
		this.update();
	}



	private addListeners() {
		this._visibleView.addEventListener("mousedown", this.onViewMouseDown);
		this._resizeLeft.addEventListener("mousedown", this.onLeftResizerMouseDown);
		this._resizeRight.addEventListener("mousedown", this.onRightResizerMouseDown);


		this._visibleView.addEventListener("touchstart", this.onViewTouchStart);
		this._resizeLeft.addEventListener("touchstart", this.onLeftResizerTouchStart);
		this._resizeRight.addEventListener("touchstart", this.onRightResizerTouchStart);

		this._visibleView.addEventListener("touchmove", this.onViewTouchMove);
		this._resizeLeft.addEventListener("touchmove", this.onViewTouchMove);
		this._resizeRight.addEventListener("touchmove", this.onViewTouchMove);

		this._visibleView.addEventListener("touchend", this.onViewTouchEnd);
		this._resizeLeft.addEventListener("touchend", this.onViewTouchEnd);
		this._resizeRight.addEventListener("touchend", this.onViewTouchEnd);
	}

	private viewData: IViewData = null;
	private initViewData(ev:MouseEvent|TouchEvent, type:ViewChangeType){
		this.viewData = {
			ev,
			delta: 0,
			type,
			shift: this.shift,
			size: this.size,
			view: this.target.getClientRects()[0],
		};
	}

	private onViewTouchStart = ev => this.touchStart(ev, ViewChangeType.Move);
	private onLeftResizerTouchStart = ev => this.touchStart(ev, ViewChangeType.ResizeLeft);
	private onRightResizerTouchStart = ev => this.touchStart(ev, ViewChangeType.ResizeRight);
	private touchStart(ev: TouchEvent, type: ViewChangeType = 0){
		this.initViewData(ev, type);
		this.busy = true;
		ev.stopPropagation();
	}

	private onViewTouchMove = ev => this.touchMove(ev, ViewChangeType.Move);
	private touchMove(ev: TouchEvent, type: ViewChangeType = 0){
		this.viewData.delta = ev.touches[0].pageX - (<TouchEvent>this.viewData.ev).touches[0].pageX;
		this.move(this.viewData);

		ev.stopPropagation();
	}

	private onViewTouchEnd = ev => this.touchEnd(ev, ViewChangeType.Move);
	private touchEnd(ev: TouchEvent, type: ViewChangeType = 0){
		this.viewData = null;
		this.busy = false;
		ev.stopPropagation();
	}

	private onViewMouseDown = ev => this.mouseDown(ev, ViewChangeType.Move);
	private onLeftResizerMouseDown = ev => this.mouseDown(ev, ViewChangeType.ResizeLeft);
	private onRightResizerMouseDown = ev => this.mouseDown(ev, ViewChangeType.ResizeRight);
	private mouseDown(ev: MouseEvent, type: ViewChangeType = 0) {
		this.initViewData(ev, type);
		/* 		
		bad performance
		this code run recalculate style event

		if(type === ViewChangeType.Move)
			document.body.classList.add("move");
		else
			document.body.classList.add("resize"); */
		this.busy = true;
		window.addEventListener("mousemove", this.onMouseMove);
		window.addEventListener("mouseup", this.onMouseUp);

		ev.stopPropagation();
	}
	private onMouseMove = ev => this.mouseMove(ev);
	private mouseMove(ev: MouseEvent) {
		//console.log(ev.x);
		this.viewData.delta = ev.x - (<MouseEvent>this.viewData.ev).x;
		this.move(this.viewData);

		ev.stopPropagation();
	}
	private onMouseUp = ev => this.mouseUp(ev);
	private mouseUp(ev: MouseEvent) {
		/*
		bad performance
		if(this.viewData.type === ViewChangeType.Move)
			document.body.classList.remove("move");
		else
			document.body.classList.remove("resize"); */
		this.busy = false;

		window.removeEventListener("mousemove", this.onMouseMove);
		window.removeEventListener("mouseup", this.onMouseUp);

		this.viewData = null;
		ev.stopPropagation();
	}
	private update() {
		if (this.onchange)
			this.onchange({
				shift: this.shift,
				size: this.size
			});
	}
}
