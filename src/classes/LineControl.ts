import { Line } from "../elements/Line";
import { LineView } from "../views/LineView";
import { MapView } from "../views/MapView";
export class LineControl {
	private _main: Line = null;
	private _mirror: Line = null;
	constructor(private _name: string, private _lv: LineView, private _mv: MapView) {
		this._main = _lv.lines.find(l => l.name === _name);
		this._mirror = _mv.lines.find(l => l.name === _name);
	}
	get name() {
		return this._name;
	}
	get visible(): boolean {
		return this._main.visible;
	}
	set visible(v: boolean) {
		this._mirror.visible = this._main.visible = v;
		this._lv.update();
		//this._mv.update();
	}
}
