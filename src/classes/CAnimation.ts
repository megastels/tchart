export class CAnimation {
	started: number = 0;
	progress: number = 1;
	private _restart: boolean = false;


	constructor(private frame: (progress: number) => void, private duration: number = 1000) { }

	private requestFrameID: number = null;
	start() {
		//var canceled = this.requestFrameID;
		this.stop();
		this.requestFrameID = requestAnimationFrame(this._frame);
		this.started = performance.now();
		
		//console.log(this);

		//console.log(`%c ID:${this.requestFrameID}, Started: ${this.started}, Canceled:${canceled}`, 'background: #222; color: white');
	}
	stop() {
		if(this.requestFrameID!==null){
			cancelAnimationFrame(this.requestFrameID);
			this.requestFrameID = 0;
		}
	}

	private _frame = (time) => {
		var ptime = performance.now();

		var progress = (ptime - this.started) / this.duration + 0.016666666666666666 // <- 1/60;
		if (progress > 1){
			progress = 1;
		}
			

		this.progress = progress;
	
		this.frame(progress);

		this.nextFrame();
	}

	nextFrame(){
		if(this.progress!==1)
			this.requestFrameID = requestAnimationFrame(this._frame);
		else
			this.requestFrameID = null;
	}

}
